<?php
error_reporting(E_ALL); ini_set('display_errors', 'On');

function debug($data){
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
}

# INCLUDE 
include 'HttpClient/BasicAuth.php';
include 'HttpClient/HttpClientException.php';
include 'HttpClient/Client.php';
include 'HttpClient/HttpClient.php';
include 'HttpClient/Options.php';
include 'HttpClient/OAuth.php';
include 'HttpClient/Request.php';
include 'HttpClient/Response.php';
include 'controllers/CheckConfigController.php';
define('LOG_FILES', 	dirname(__FILE__). "/logs");

# HANDLE ROUTING
if(!isset($webshop)){
		$url = explode("/",$_SERVER['REQUEST_URI']);
		$webshop = $url[2];
		
		if($_SERVER['SERVER_ADDR'] == "[ip.address]"){
				
				define('ENV',	'test');
				$subPath = "/API-Manager-test";
		}
		else{
				
				define('ENV',	'prod');
				$subPath = "/API-Manager";
		}
		
		$path = realpath(dirname(dirname(__FILE__))) . $subPath; 
}

if(!file_exists($path.'/webshop/' . $webshop . ".php")){

	echo  'configuration was not found in the system for: "' . $webshop .'"';
	exit;
}

# INCLUDE THE CONFIG CLASS OF THE CURRENT WEBSHOP
include 'webshop/' . $webshop . ".php";

if(strpos($webshop, "ws-db") != false){

	$webshop = explode("-",$webshop);
	$webshop = $webshop[0] . "WStoDB" ;

}

# THE CURRENT WEBSHOP NAME
define('WEBSHOP', $webshop);
# INTIALIZE THE CONFIG CLASS
$ConfController = $webshop . "ConfController"; 
$Ctrl = new  $ConfController();


# CHECK BASIC SETTINGS FOR WEBHOP
# if something was not set properly or something is missing from the configuration
# we should stop the procedure
$checkConfCtrl = new CheckConfigController();
$checkRes = $checkConfCtrl->checkConfigurations($Ctrl);

if(!$checkRes){
	exit;
}

# INCLUDE THE CLASS WHICH HANDLES THE ORDERS FROM THE WEBSHOP
include 'controllers/'.$Ctrl->webshop.'Controller.php';
# INITIALIZE THE HANDLER
$WebshopController = $Ctrl->webshop . "Controller";

# INCLUDE THE  CONTROLLER FOR PERFECT BASE
include 'controllers/ErrorHandlerController.php';
include 'controllers/HTTP_Requester.php';
include 'controllers/PerfectBaseController.php';
include 'controllers/DHL_Controller.php';
include 'controllers/DatabaseController.php';
# START THE PROCESS
new $WebshopController();



