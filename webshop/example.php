<?php

#	=====	DEFINE THE CLASS	===== #
#  This file contains all the condiguration which needed to get the orders from the webshop 
#	The class name should be as defined : 
#	[ name of the shop (it comes from the url) ] + ConfController	//	API-manager/example
	
class exampleConfController { 

				#	===== FIXED DATA ===== #
				#	All the fields which should be stored in the database defined with default data in the webshop controller
				#	All the fields can be overwritten by defining the "$fixedData" as a public property
				#	it's an array of data 
				#
				#	key => Name of the mysql databse field
				#	value => The value should be stored
				public $fixedData = array(

						"ALG_ID" => 11,
						"GROUP_ID" => 15

				);
	
				#	===== CURRENT WEBSHOP ENVIRONMENT =====	#
				#	The current webshop environment has to be defined with the name of the webshop 
				#	as a public property "$webshop"
				#	WooCommerce, Magento etc..

				public $webshop = "WooCommerce";
				public $action  = "DHL";

				# ===== FETCH ORDERS BY STATUS ============= #
				# processing, on-hold, completed, cancelled, refunded and failed , test
				# status in WS to get
				public $status_to_get = "processing"; 
				# status in WS to set
				public $status_to_set = "completed";


				public function setCredentials() {

						if($_SERVER['SERVER_ADDR'] == '[server.ip.address]'){
								# TEST #
								define('WC_KEY', "test_key_API");
								define('WC_SECRET', "test_secret_API");
								define('WC_URL', "test_webshop_url");

						}
						else{
								# PRODUCTION #
								define('WC_KEY', "test_key_API");
								define('WC_SECRET', "prod_secret_API");
								define('WC_URL', "prod_webshop_url");

						}			
				}

				#	===== SELECTED DATA TO STORE===== #
				#	All the fields which should be selected from the returned data set and stored in our databse
				#	it's a multiarray of data 
				#
				#	key => Name of the mysql table 
				#	value => An array of data where
				#										=> key is the data to pick from the returned array
				# 										=> value is the name of the field in the mysql database
				#
				# 'fixed_data' => stores a value which should be stored statically in our db
				#										=> key  is the name of the field in the mysql database
				# 										=> value is the static value should be stored

				public $aDataToStore = array(
						
						# the "*" indicates the name of the parent array element's name
						# 'billing*first_name'  in this case is   ["billing"]["last_name"]
						"Orders" => array(
								
								'id' => 'Ordernummer',	
								'date_created' => 'Complete_datum' ,
								'shipping*first_name' => 'Voornaam' , 
								'shipping*last_name' => 'Achternaam' , 
								'_shipping_street' => 'Straat' ,
								'_shipping_houseno' => 'Huisnummer' , 
								'_shipping_extension' => 'Toevoeging' ,
								'_wcpdf_invoice_number' => 'Factuur_nr' ,
								'shipping*postcode' => 'Postcode' ,
								'shipping*city' => 'Plaats' ,
								
								'fixed_data' => array (
										'Verzendmethode' => 'DHL'
 								)

						),
							
						"OrderLines" => array(
								
										'line_items*sku' => 'sku' ,
										'line_items*[parent]id' => 'OrdID' ,
										'line_items*name' => 'omschrijving' ,
										'line_items*product_id' => 'itemID' ,
										'line_items*quantity' => 'aantal' , 
										'line_items*maat' => 'Maat', 
										'line_items*shipping_class' => 'prod_shipping_class',
							)

				);

}