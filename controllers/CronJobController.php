<?php
function debug($data){
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
}
include "ConfigController.php";

class	CronJobController	{
		
				public $db;
				public $CronJobs;
			

				public function __construct($webshop) {

								$this->execute($webshop);
								$this->Error = new ErrorHandlerController('cron');

				}

				public function execute($webshop){
						
						$ConfigController = new ConfigController();
						$ConfigController->runWebshopController($webshop);
						
						exit();

				}
		
}
