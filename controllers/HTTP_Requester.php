<?php
	
class HTTPRequester {

		public  function HTTPPost($url, $params) {

				try{
						    $options = array('trace' => true, 'soap_version' => SOAP_1_2);
											
								$soap = new SoapClient($url, $options);
								$response = $soap->__SoapCall('PGPDMWSHOP_TBLORDWS_ADD', array($params));
								
								$error = strlen($response->PGPDMWSHOP_TBLORDWS_ADDResult->ErrorString) > 0 ? $response->PGPDMWSHOP_TBLORDWS_ADDResult->ErrorString : false;
								$insertedID = $response->PGPDMWSHOP_TBLORDWS_ADDResult->TBLORDWS_ID;

								
								// if ErrorString
								if($error || $insertedID == 0){
//												die($error);
										// do not update
										return false;
								}
							
								return $insertedID;
								
				} catch (Exception $ex) {
								// die($ex);
								return false;
				}	
		}
}