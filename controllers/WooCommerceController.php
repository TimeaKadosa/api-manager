<?php

class WooCommerceController{
	
		public $sOrderLines = "";
		public $aOrders = array();

		public function __construct($webshop = false) {

				
				$this->Error = new ErrorHandlerController('error');
				# GET CURRENT CONFIGURATIONS
				
				if($webshop){
						$controllerName = $webshop."ConfController";
				}
				else{
						$controllerName = WEBSHOP."ConfController";
				}
				
				$this->WebshopConf= new $controllerName();
				# SET WEBSHOP CREDENTIALS 
				$this->WebshopConf->setCredentials();
				
				# =====	START THE PROCEDURE	===== #
				# CONNETC TO THE CURRENT WEBSHOP
				$this->connectWebshop();
				
				$action = $this->WebshopConf->action;
				
				# CALL THE RELATED FUNCTION
				$method_action = "action_". $action;
				$this->$method_action();
				

		}
		
		public function action_PB(){
						# SET FIXED DATA SHOULD BE STORED
						$this->fixedData = $this->WebshopConf->fixedData;

						# INITIALIZE THE CONTROLLER FOR PERFECT BASE
						$this->PB_Controller = new PerfectBaseController();

						# THE FUNCTION GETS AND SETS THE DATA IN A WAY SHOULD BE STORED
						$this->prepareDataToStore();
						# SAVE THE PREVIOUSLY PREPARED DATA
						$this->saveToPB();
		}
		
		public function action_DHL(){

						# SET CURRENT STATUS TO GET
						$status = $this->WebshopConf->status_to_get;

						# INITIALIZE THE CONTROLLER FOR PERFECT BASE
						$this->DHL_Controller = new DHL_Controller();
						# ===== FETCH ORDERS BY STATUS ============= #
						# processing, on-hold, completed, cancelled, refunded and failed
						$aOrders = $this->getOrders($status);

						if($aOrders != false){
								$this->DHL_Controller->setDataToStore($aOrders,$this->woocommerce,  $this->WebshopConf->meta_data_keys);
								
						}
						else{
									echo "No orders were found";
									exit;
						}
						

		}
		
		public function  action_WS_DB(){
						
						# SET CURRENT STATUS TO GET
						$status = $this->WebshopConf->status_to_get;
						# DATABASE IN PG-AKP TO STORE THE DATA
						$db = $this->WebshopConf->database;
						# ===== FETCH ORDERS BY STATUS ============= #
						# processing, on-hold, completed, cancelled, refunded and failed , wc-test
						$aOrders = $this->getOrders($status);

						# store the orders in the corresponding database
						# INITIALIZE THE CONTROLLER FOR DATABASE
						$this->DatabaseController = new DatabaseController();
						# change the status from procesing to pg-processing					
						if($aOrders != false){

								# Create a batch. It returns the batch "id" which was just created
								//$batchID = $this->DatabaseController->insertBatch(count($aOrders));
								
								foreach	($aOrders as $aOrder){


										$orderExists = $this->DatabaseController->checkOrderExists($aOrder['id']);

										if($orderExists == false){

												$aOrder = $this->getShippingClasses($aOrder);
											
												$res = $this->DatabaseController->storeData($aOrder);

												if(!$res){
														$error = "Order could not be stored, during an error id:" . $aOrder['id'];
														$this->Error->logError($error);
												}
												else{

														$this->changeStatus($aOrder['id']);
												}

										}
										else{
												$error = "Order could not be stored, duplicate id:" . $aOrder['id'];
												$this->Error->logError($error);

										}
								}
						}
		}
	
		public function connectWebshop() {

				$options = array(
								'wp_api' => true,
								'version' => 'wc/v2',
								'ssl_verify'      => false,
							//	'debug'           => false,
				);
	
						$this->woocommerce = new Client(
																WC_URL,
																WC_KEY,
																WC_SECRET,	
																$options 
						);	
		}
	
		public function prepareDataToStore(){
					# ===== FETCH ORDERS BY STATUS ============= #
					# processing, on-hold, completed, cancelled, refunded and failed
					# SET CURRENT STATUS TO GET
					$status = $this->WebshopConf->status_to_get;
					$aOrders = $this->getOrders($status);

					echo "<h3>Orders : Response from WooCommerce</h3>";
					$this->setData($aOrders);
		}


		public function getOrders($sStatus) {
					# ===== ORDER STATUS =============
					# processing, on-hold, completed, cancelled, refunded and failed
				

					$aOrders = $this->woocommerce->get('orders', array('status' => $sStatus));

					if( count($aOrders) > 0 ){
						return $aOrders;
					} 
					else{
						return false;
					}
		}
		
		public function changeStatus($orderID){
				
						# SET CURRENT STATUS TO GET
						$status = $this->WebshopConf->status_to_set;
						$data = array(
										'status' => $status
						);
						# change the order status in woocommerce
						$this->woocommerce->put('orders/'.$orderID, $data);
				
		}
	
		public function setData($aOrders){

			if($aOrders){

					foreach ($aOrders  as $index => $order){

							# HOLD CURRENT ORDER IN A TEMP ARRAY
							$this->aOrders[$index] = $order;
							# ATTACH THE ORDER LINES
							$this->setOrderLines($order["line_items"], $index);
							# ATTACH THE ADDITIONAL DATA TO THE CURRENT ORDER 
							$this->setMetaData($index, $order);
					}
				}
				else{
					echo "No orders were found";
				}	
		}

	
		public function setMetaData($index, $aOrder){

				# SET DEFAULT
				$this->aOrders[$index]['billing']['_billing_Hnr'] = "";
				$this->aOrders[$index]['billing']['_billing_HnrT'] = "";
				$this->aOrders[$index]['shipping']['_shipping_Hnr']  = "";
				$this->aOrders[$index]['shipping']['_shipping_HnrT']  = "";

				# SET VALUE IF ANY
				foreach ($aOrder["meta_data"] as $key => $data){

						# BILLING HUISNUMMER
						if(in_array('_billing_Hnr', $data) || in_array('_billing_hnr', $data)  ){

							$this->aOrders[$index]['billing']['_billing_Hnr'] = $aOrder['meta_data'][$key]['value'];
						}
						

						# BILLING HUISNUMMER TOEVOEGING	 / billing_hnr_tv
						if(in_array('_billing_HnrT', $data ) || in_array('_billing_hnr_tv', $data)){

							$this->aOrders[$index]['billing']['_billing_HnrT'] = $aOrder['meta_data'][$key]['value'];
						}

						# SHIPPING HUISNUMMER
						if(in_array('_shipping_Hnr', $data) || in_array('_shipping_hnr', $data)){

							$this->aOrders[$index]['shipping']['_shipping_Hnr']  = $aOrder['meta_data'][$key]['value'];
						}

						# SHIPPING HUISNUMMER TOEVOEGING
						if(in_array('_shipping_HnrT', $data) || in_array('_shipping_hnr_tv', $data)){

							$this->aOrders[$index]['shipping']['_shipping_HnrT'] =$aOrder['meta_data'][$key]['value'];
						}
				}
		}
	
	
		public function setOrderLines($aOrderLines, $index) {

				$this->aOrders[$index]['OrderLines'] = "";
				$changePrice = false;
				
				foreach ($aOrderLines as $key => $line) {
						# sku defined with pipe
						if(strlen($line['sku']) > 0 && strpos( $line['sku'], '|' ) > 0 ){

								$bundle = false;
								
								foreach	($line["meta_data"] as $meta){

										if($meta['key'] == 'woosb_parent_id'){
												$parentID = $meta['value'];
												$bundle = true;
										}
								}
								
								if($bundle){
										
										foreach	($aOrderLines as $lineItem){
								
												if($lineItem['product_id'] == $parentID){

															$price_total = $lineItem['price'];
															$price_total_1 = $lineItem['total'];
												}
										}

										$changePrice = true;
								}
								
								$bundle = false;
						}
						
						if($changePrice){

								$price = $price_total;
								$total = $price_total_1;					

						}
						else{
								
								$price =		$line['price'];
								$total =		$line['total'];
						}
						
						$changePrice = false; 

						$total = number_format((float) $total + $line["total_tax"], 2, '.', '');
						$price = $total / $line["quantity"];

						$this->aOrders[$index]['OrderLines'] .= $line['id'] ."#". $total . "#" . $price . "#" . $line['quantity'] . "#" .  $line['tax_class'] . "#" .  str_replace("#"," ",$line->name ) . "#" . $line['sku'] .  ";" ;
	

						}
				
				$shipping = "";
				
				if($this->aOrders[$index]["shipping_total"] > 0) {
							
						$shipping_total =$this->aOrders[$index]["shipping_total"] + $this->aOrders[$index]["shipping_tax"]; 
						$shipping ="#". $shipping_total . "#".$shipping_total."#1##Toeslag verzendkosten#850510;";
						$this->aOrders[$index]['OrderLines'] .= $shipping;
				}

				$this->aOrders[$index]['OrderLines'];

		}
	
	
		public function saveToPB(){

				foreach ($this->aOrders as $order) {
					# STORE THE PREPARED DATA TO PB
					$result  = $this->PB_Controller->saveOrder($order,$this->fixedData);	

					if($result){
						$this->changeStatus($order['id']);			
					}
				}
		}
		
		public function getShippingClasses($aOrder){

				foreach	($aOrder["line_items"] as $key => $line){

						$product = $this->woocommerce->get('products/' . $line["product_id"]);
								
						$aOrder["line_items"][$key]['shipping_class'] = $product["shipping_class"];
						$aOrder["line_items"][$key]['shipping_class_id'] = $product["shipping_class_id"];
				}
				
				return $aOrder;

		}
	
}
