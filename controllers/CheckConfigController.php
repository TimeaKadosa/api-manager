<?php

	class CheckConfigController {
		
		public function checkConfigurations($Ctrl){

				$CtrlAction = $Ctrl->action;

				$propertyWebshop	= property_exists($Ctrl, 'webshop');
				$methodSetCredetials = method_exists($Ctrl, 'setCredentials');

				$errors = 0;
			
				if(!$propertyWebshop) {

					echo 'Class property : "$webshop" should be defined ! </br> Accepts a string and should be defined as a public property. <hr>' ;
					$errors++;
				}
			
				if(!$methodSetCredetials) {

						echo 'Class method : "setCredentials" is missing ! </br><hr>' ;
						$errors++;
				}

						# CHECK FOR PB #
						if($CtrlAction == "PB"){

										$propertyFixedData	= property_exists($Ctrl, 'fixedData');			
										if(!$propertyFixedData) {

												echo 'Class property : "$fixedData" should be defined ! </br> Accepts an array and should be defined as a public property. <hr>' ;
												$errors++;
										}
						}
			
						# CHECK FOR DHL #
						if($CtrlAction == "DHL"){

										$meta_data_keys	= property_exists($Ctrl, 'meta_data_keys');			
										if(!$meta_data_keys) {

												echo 'Class property : "$meta_data_keys" should be defined ! </br> Accepts an array and should be defined as a public property. <hr>' ;
												$errors++;
										}
						}
			
						if($errors == 0){
								return true;
						}
						else{
								return false;
						}

		}
		
	}