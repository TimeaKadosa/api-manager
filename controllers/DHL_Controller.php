<?php

	class DHL_Controller {
	
		
		public function __construct() {
				
						$this->connectDB();
		}

		public function connectDB(){

					$this->db = new PDO('mysql:host=[host];dbname=[db];charset=utf8', '[user]', '[password]');
					$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		
		public function setDataToStore($orders, $woocommerce, $meta_data_keys) {
				
						foreach ($orders as $order) {

								$aOrderData = array();
								$aOrderData['ZndUsrID'] = 19;
								$aOrderData['ZndBdrID'] = 37;
								$aOrderData['ZndDatum'] = date('Y-m-d');
								$aOrderData['ZndNaam'] = $order['shipping']['first_name']. " ". $order['shipping']['last_name'] . " ". $order['shipping']['company'];
								$aOrderData['ZndStraat'] = $order['shipping']['address_1'];

								$aOrderData['ZndPostcode'] = $order['shipping']['postcode'];
								$aOrderData['ZndPlaats'] = $order['shipping']['city'];

								foreach ($order['meta_data'] as $meta) {

										if($meta['key'] == $meta_data_keys['shipping_Hnr']){

												$aOrderData['ZndHnr'] = $meta['value'];

										}

										elseif($meta['key'] == $meta_data_keys['shipping_Hnr_extension']) {
												$aOrderData['ZndHnrT'] = $meta['value'];
										}

										elseif($meta['key'] == '_wcpdf_invoice_number'){
												$aOrderData['ZndBarcode'] = "3SDMG". $meta['value'];
										}

								}


								if(count($order['line_items']) == 1) {

												$prodId = $order['line_items'][0]['product_id'];
												$product = $woocommerce->get('products/' . $prodId );
												$prodClass = $product['shipping_class'];

												if($prodClass == 'bus'){
														$ZndPrdID = 5;
												}
												else{
														$ZndPrdID = 1;
												}

												$aOrderData['ZndPrdID'] = $ZndPrdID;
								}
								else{
												$aOrderData['ZndPrdID'] = 1;
								}

								$aOrderData['ZndGeprint'] = 'J';
								$this->saveOrder($aOrderData);
								
								$this->changeStatusWC($order['id'], $woocommerce);

						}
		}
		
		public function changeStatusWC($orderID, $woocommerce){
				
						$data = array(
										'status' => 'completed'
						);

						# change the order status in woocommerce
						$woocommerce->put('orders/'.$orderID, $data);
				
		}


		public function saveOrder($aOrderData){
				
						extract($aOrderData);

						$sql = "INSERT INTO tblZendingen (ZndUsrID, ZndBdrID, ZndDatum, ZndNaam, ZndStraat, ZndPostcode, ZndPlaats, ZndBarcode, ZndHnr, ZndHnrT, ZndPrdID, ZndGeprint) 
								VALUES (:ZndUsrID, :ZndBdrID, :ZndDatum, :ZndNaam, :ZndStraat, :ZndPostcode, :ZndPlaats, :ZndBarcode, :ZndHnr, :ZndHnrT , :ZndPrdID , :ZndGeprint)";

						$smtp = $this->db->prepare($sql);

						$smtp->bindParam(':ZndUsrID', $ZndUsrID);
						$smtp->bindParam(':ZndBdrID', $ZndBdrID);
						$smtp->bindParam(':ZndDatum', $ZndDatum);
						$smtp->bindParam(':ZndNaam', $ZndNaam);
						$smtp->bindParam(':ZndStraat', $ZndStraat);
						$smtp->bindParam(':ZndPostcode', $ZndPostcode);
						$smtp->bindParam(':ZndPlaats', $ZndPlaats);
						$smtp->bindParam(':ZndBarcode', $ZndBarcode);
						$smtp->bindParam(':ZndHnr', $ZndHnr);
						$smtp->bindParam(':ZndHnrT', $ZndHnrT);
						$smtp->bindParam(':ZndPrdID', $ZndPrdID);
						$smtp->bindParam(':ZndGeprint', $ZndGeprint);

						$smtp->execute();
						$db = null;

		}

	}
	
