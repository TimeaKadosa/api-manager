<?php

	class DatabaseController {
	
				public $db_name;
				public $db_user;
				public $db_password;


				public function __construct() {
								# GET CURRENT CONFIGURATIONS
								$controllerName = WEBSHOP."ConfController";
								$this->WebshopConf= new $controllerName();

								$this->db_name =  $this->WebshopConf->database;
								$this->connectDB();

								$this->Error = new ErrorHandlerController('database');

		//						$this->WC = new WooCommerceController();
				}

				public function connectDB(){

							$this->db = new PDO('mysql:host=[host];dbname=[db];charset=utf8', "[user]", "[password]");
							$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				}

		
				public function getRequiredFields($table){

						$sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->db_name."' AND TABLE_NAME = 'Orders' AND IS_NULLABLE = 'NO'";


						$smtp = $this->db->prepare($sql);
						$smtp->execute();		
						return $smtp->fetchAll(PDO::FETCH_ASSOC);	


				}

				public function checkNrOfFields($table,$dataToStore){

						// debug($this->WebshopConf->aDataToStore[$table]);
						$notNullFields = $this->getRequiredFields($table);

						$missingFields = array();
						
						# match the fields from db which can not be null with the fields which should be stored
						foreach ($notNullFields as $value) {

									# the following fields should be stored in the db by the webshop settings
									if(in_array($value['COLUMN_NAME'], $this->WebshopConf->aDataToStore[$table])){

											if(!array_key_exists($value['COLUMN_NAME'], $dataToStore)){

													array_push($missingFields, $value['COLUMN_NAME']);
											}
									}
						}


						return $missingFields;
				
				}

				public function insertData($dataToStore, $table) {

						$missingFields = $this->checkNrOfFields($table, $dataToStore);

						if(count($missingFields) > 0 ){

							$error = "Order ID : ".$dataToStore['Ordernummer']." Error occured because of the missing fields : " . json_encode($missingFields);
							$this->Error->logError($error);
								
							return false;
						}
						else{

								$aValues = array();
						
								foreach	(array_values($dataToStore) as $key => $value){
										array_push($aValues,	trim(str_replace("'", " " , $value)));
								}
								
								// select Ordernummer from Orders group by Ordernummer having count(*) >1
								//  iconv("UTF-8", "ISO-8859-1", $data['shipping_firstname']);
								$sValues = implode($aValues, "','");
								$sKeys = implode(array_keys($dataToStore), ",");

								$sql = 	
														"INSERT INTO " . $table .
														" (". $sKeys .")" .
														" VALUES ('" . $sValues. 
														"')";
								$smtp = $this->db->prepare($sql);

								try {
										$res = $smtp->execute();		
										

								} catch (Exception $e) {

										$this->Error->logError($e->getMessage());
										$res = false;
								}

								return $res;
						}

						
				}

				public function insertBatch($total){

							$sql = 	
												"INSERT INTO Batch " .
												" (aantal)" .
												" VALUES ('" . $total . 
												"')";
						$smtp = $this->db->prepare($sql);
						$smtp->execute();

						return (int)$this->db->lastInsertId();
				}

				# store the order
				public function storeData($aOrder){


						# the desired data should be stored in the database
						$aDataToStore = $this->WebshopConf->aDataToStore;

						foreach	($aDataToStore as $table => $tableData){

										# temp array which holds the data will be stored 
										$dataToDB = array();

											# loop through the config array to get the field names
										foreach	($tableData as $wc_key => $db_key){

												# if $wc_key does not contain "*", we are dealing with the root of the array
												if	(strpos(	$wc_key, "*") == false)	{
														$wc_keys[0] = $wc_key;
												}
												# if $wc_key has "*" in it, we are dealing with a subarray
												else{
														$wc_keys = explode("*",	$wc_key);	
												}
												
												# get the data from the root of the array
												if(count($wc_keys) == 1){					

															# it is possible that the key was defined from the "meta_data" of the root array
															if(!array_key_exists($wc_keys[0],	$aOrder)){

																	# if the key is not in the root array than it should be from the meta_data
																	foreach	($aOrder['meta_data'] as $metaData){

																			if($metaData['key'] == $wc_keys[0]){
																									$dataToDB[$db_key] = $metaData['value'];	
																			}
																	}
															}
															# store the data from the root array
															else{
																		$dataToDB[$db_key] = $aOrder[$wc_keys[0]];		
															}

												}
												# get the data from a subarray ( it is not the meta_data ! )
												# root array can have subarray which is multidimensional like "line_items"
												else{

														# if the subArray is an idexed array we have to perform an extra loop		
														if( !$this->isAssociativeArray($aOrder[$wc_keys[0]])){

																foreach	($aOrder[$wc_keys[0]] as $key => $subArray){

																		# in case we run into a key which is not in the array
																		if(!array_key_exists($wc_keys[1],	$aOrder[$wc_keys[0]][$key])){

																						# check if we should get the data from the parent(root) array
																						if(strpos(	$wc_keys[1], "[parent]") === 0){
																								
																								$parentKey = explode('[parent]', $wc_keys[1]);
																								$dataToDB[$key][$db_key] = $aOrder[$parentKey[1]];
																						}
																						# or from the meta_data
																						else{
																								# if the key is not in the root array than it should be from the meta_data
																								foreach	($aOrder[$wc_keys[0]][$key]['meta_data'] as $metaData){

																										if($metaData['key'] == $wc_keys[1]){

																													$dataToDB[$key][$db_key] = $metaData['value'];	
																										}
																								}
																						}																					
																		}
																		# the data can be stored from the subarray to the indexed array
																		else{
																					$dataToDB[$key][$db_key] = $aOrder[$wc_keys[0]][$key][$wc_keys[1]];	
																		}
																}
														}
														else{
																# the data can be stored from the array ($dataToDB)
																$dataToDB[$db_key] = $aOrder[$wc_keys[0]][$wc_keys[1]];	
														}		

												}	
												
														# clean up the array of wc_keys
														$wc_keys = array();															
												}
												
												# if we have multi array 
												if($this->isMultidimensionalArray($dataToDB)){
														
																$dataToDB =		array_values(array_filter($dataToDB));

																foreach	($dataToDB as $aData){
																		

																		if(array_key_exists('fixed_data',	$tableData)){
																				foreach	($tableData['fixed_data'] as $fixedKey => $fixedValue){
																						$aData[$fixedKey] = $fixedValue;
																				}
																		}

																		$insert = $this->insertData($aData, $table);

																		if(!$insert){
																			return false;
																		}

																}
												}
												else{


														if(array_key_exists('fixed_data',	$tableData)){
																foreach	($tableData['fixed_data'] as $fixedKey => $fixedValue){
																		$dataToDB[$fixedKey] = $fixedValue;
																}
														}

														$insert = $this->insertData($dataToDB, $table);
																	if(!$insert){
																			return false;
																		}

	
												}
									
												# clean up the array
												$dataToDB = array();		
							}

							return true;
				}

				public function checkOrderExists($OrdNr){

						$sql = "SELECT * FROM Orders WHERE Ordernummer = " . $OrdNr;
						$smtp = $this->db->prepare($sql);
						$smtp->execute();

 						return $smtp->fetch(PDO::FETCH_ASSOC);	

				}
		
				public function isMultidimensionalArray(array $array) {
								return count($array) !== count($array, COUNT_RECURSIVE);
				}

				public function isAssociativeArray(array $arr)
				{
								if (array() === $arr) return false;
								return array_keys($arr) !== range(0, count($arr) - 1);
				}
	
	}
	
