<?php 

class ErrorHandlerController
{
	
	function __construct($sType)
	{
			$this->LogFile = $this->setLogFile($sType);
	}

	public function logError($sError) {

			$timestamp = date("Y-m-d H:i:s");
			$log = $timestamp ." " . $sError ." \n";

			file_put_contents($this->LogFile, $log, FILE_APPEND | LOCK_EX);
	}

	public function setLogFile($sType) {

			switch ($sType) {

				case 'database':
					$logFile = LOG_FILES.'/error_database.txt';
					break;

				case 'error':
					$logFile = LOG_FILES.'/error.txt';
					break;

				default:
					# code...
					break;
			}
			
			return $logFile;
	}

}