<?php
				include '../HttpClient/BasicAuth.php';
				include '../HttpClient/HttpClientException.php';
				include '../HttpClient/Client.php';
				include '../HttpClient/HttpClient.php';
				include '../HttpClient/Options.php';
				include '../HttpClient/OAuth.php';
				include '../HttpClient/Request.php';
				include '../HttpClient/Response.php';
				include 'CheckConfigController.php';
				
				# INCLUDE THE  CONTROLLER FOR PERFECT BASE
				include 'ErrorHandlerController.php';
				include 'HTTP_Requester.php';
				include 'PerfectBaseController.php';
				include 'DHL_Controller.php';
				include 'DatabaseController.php';
				

				define('LOG_FILES', 	"/var/www/html/API-Manager/logs");
				# INCLUDE WOOCOMMERCE  API CLIENT
				//include 'services/class-wc-api-client.php';

				if($_SERVER['SERVER_ADDR'] == "[ip.address]"){

						define('ENV',	'test');
						define('DOC_ROOT',	$_SERVER["DOCUMENT_ROOT"].'/API-Manager');
						define('WEB_ROOT',	'/API-Manager');
						
						$subPath = "/API-Manager";
				}
				else{

						define('ENV',	'prod');
						define('DOC_ROOT',	$_SERVER["DOCUMENT_ROOT"].'/API-Manager');
						define('WEB_ROOT',	'/API-Manager');

				}
				
class ConfigController{
		
		
		public function __construct()	{

				//$this->runWebshopController();
		}

		public function runWebshopController($webshop){
				
						if(!file_exists(DOC_ROOT.'/webshop/' . $webshop . ".php")){

							echo  'configuration was not found in the system for: "' . $webshop .'"';
							exit;
						}

						# INCLUDE THE CONFIG CLASS OF THE CURRENT WEBSHOP
						include DOC_ROOT.'/webshop/' . $webshop . ".php";

						if(strpos($webshop, "ws-db") != false){

							$webshop = explode("-",$webshop);
							$webshop = $webshop[0] . "WStoDB" ;

						}

						# THE CURRENT WEBSHOP NAME
						$GLOBALS['webshop'] = $webshop;
						# INTIALIZE THE CONFIG CLASS
						$ConfController = $webshop . "ConfController"; 
						$Ctrl = new  $ConfController();


						# CHECK BASIC SETTINGS FOR WEBHOP
						# if something was not set properly or something is missing from the configuration
						# we should stop the procedure
						$checkConfCtrl = new CheckConfigController();
						$checkRes = $checkConfCtrl->checkConfigurations($Ctrl);

						if(!$checkRes){
							exit;
						}
						
						if(!class_exists($Ctrl->webshop . "Controller")){
								# INCLUDE THE CLASS WHICH HANDLES THE ORDERS FROM THE WEBSHOP
								include DOC_ROOT.'/controllers/'.$Ctrl->webshop.'Controller.php';
								# INITIALIZE THE HANDLER							
						}
						
										$WebshopController = $Ctrl->webshop . "Controller";
										# START THE PROCESS
										new $WebshopController($webshop);		


		}
}
