<?php

class ClientManager {

	function __construct() {
		
			# set controllers for later usage
			$this->DataController = new DataController();
			$this->PerfectBase = new PerfectBase();

			# start the process
			$this->processData();
	}
	
	public function processData() {
		
			# get orders
			$aData = $this->DataController->getData();
			# set data to store in PB
			$dataToStore = $this->setData($aData);
			# convert the array of data to xml
			$xml = $this->DataController->getXML($dataToStore);
			# send data to PB
			//$this->PerfectBase->xml_send($xml);
	}
	
	public function setData($aData) {
		
			$aOrders = array();

			foreach ($aData['orders'] as  $key => $order) {

				$aOrders['algopd_id'] = 15;
				$aOrders['batch_id'] = 1;
				$aOrders['batch_date'] = date("Y-m-d") . "T" . date("H:i:s") . "Z";
				$aOrders['total'] = count($aData['orders']);
				$aOrders['orders']['order_' . $key]['id'] = $order['id'];
				$aOrders['orders']['order_' . $key]['completed_at'] =  $order['completed_at'];
				$aOrders['orders']['order_' . $key]['payment_method'] = $order['payment_details']['method_title'];


				foreach ($order['line_items'] as $index =>$line_item){

						# ORDER LINES
						$aOrders['orders']['order_' . $key]['line_items'][$index]['id']= $line_item['id'];
						$aOrders['orders']['order_' . $key]['line_items'][$index]['total']= $line_item['total'];
						$aOrders['orders']['order_' . $key]['line_items'][$index]['price']= $line_item['price'];
						$aOrders['orders']['order_' . $key]['line_items'][$index]['quantity']= $line_item['quantity'];
						$aOrders['orders']['order_' . $key]['line_items'][$index]['tax_class']= $line_item['tax_class'];
						$aOrders['orders']['order_' . $key]['line_items'][$index]['name']= $line_item['name'];
						$aOrders['orders']['order_' . $key]['line_items'][$index]['sku']= $line_item['sku'];
				}

				# CUSTOMER
				$aOrders['orders']['order_' . $key]['customer']['id'] = $order["customer"]['id'];
				// $aOrders['orders']['order_' . $key]['customer']['created_at'] = $order["billing_address"]['company'];
				$aOrders['orders']['order_' . $key]['customer']['first_name'] =$order["billing_address"]['first_name'];
				$aOrders['orders']['order_' . $key]['customer']['last_name'] = $order["billing_address"]['last_name'];
				$aOrders['orders']['order_' . $key]['customer']['company'] = $order["billing_address"]['company'];
				$aOrders['orders']['order_' . $key]['customer']['email'] = $order["billing_address"]['email'];
				$aOrders['orders']['order_' . $key]['customer']['phone'] = $order["billing_address"]['phone'];

				# CUSTOMER / BILLING ADDRESS
				$aOrders['orders']['order_' . $key]['customer']['billing_address']['street'] = $order["billing_address"]['address_1'];
				$aOrders['orders']['order_' . $key]['customer']['billing_address']['city'] = $order["billing_address"]['city'];
				$aOrders['orders']['order_' . $key]['customer']['billing_address']['postcode'] = $order["billing_address"]['postcode'];
				$aOrders['orders']['order_' . $key]['customer']['billing_address']['country'] = $order["billing_address"]['country'];

				# CUSTOMER / SHIPPING ADDRESS
				$aOrders['orders']['order_' . $key]['customer']['shipping_address']['first_name'] =  $order["shipping_address"]['first_name'];
				$aOrders['orders']['order_' . $key]['customer']['shipping_address']['last_name'] =  $order["shipping_address"]['last_name'];
				$aOrders['orders']['order_' . $key]['customer']['shipping_address']['company'] =  $order["shipping_address"]['company'];
				$aOrders['orders']['order_' . $key]['customer']['shipping_address']['street'] = $order["shipping_address"]['address_1'];
				$aOrders['orders']['order_' . $key]['customer']['shipping_address']['city'] = $order["shipping_address"]['city'];
				$aOrders['orders']['order_' . $key]['customer']['shipping_address']['postcode'] = $order["shipping_address"]['postcode'];
				$aOrders['orders']['order_' . $key]['customer']['shipping_address']['country'] = $order["shipping_address"]['country'];

			}


			#######  TEST ADDITIONALS ###########
//						$aOrders['orders']['order_' . $key]['line_items'][1]['id']= 1;
//						$aOrders['orders']['order_' . $key]['line_items'][1]['total']= 21;
//						$aOrders['orders']['order_' . $key]['line_items'][1]['price']= 22;
//						$aOrders['orders']['order_' . $key]['line_items'][1]['quantity']= 33;
//						$aOrders['orders']['order_' . $key]['line_items'][1]['tax_class']= 12;
//						$aOrders['orders']['order_' . $key]['line_items'][1]['name']= "something";
//						$aOrders['orders']['order_' . $key]['line_items'][1]['sku']= "JKU-123";
			#######  TEST ADDITIONALS ###########


			return $aOrders;

	}

}